#include "get_next_line_bonus.h"

#include <stddef.h>
#include <stdlib.h>

static t_gnl_ret	gnl_save(t_gnl_save *save, char **line, size_t *size)
{
	char	*buf;
	size_t	n;

	if (save->bytes_read != -1 && save->nl_idx + 1 < (size_t)save->bytes_read)
	{
		buf = save->buf + (save->nl_idx + 1);
		n = (size_t)save->bytes_read - (save->nl_idx + 1);
		*size = memchridx(buf, '\n', n);
		save->nl_idx += 1 + *size;
		if (*size != 0)
		{
			*line = malloc(*size);
			if (*line == NULL)
				return (e_gnl_ret_err);
			ft_memcpy(*line, buf, *size);
		}
		else
			*line = NULL;
		if (*size != n)
			return (e_gnl_ret_found);
		return (e_gnl_ret_eof);
	}
	*line = NULL;
	*size = 0;
	return (e_gnl_ret_eof);
}

static t_gnl_ret	gnl_fd_ret(t_gnl_save *save, char **line)
{
	if (save->bytes_read == -1)
	{
		free(*line);
		*line = NULL;
		return (e_gnl_ret_err);
	}
	return (e_gnl_ret_eof);
}

static t_gnl_ret	gnl_fd(int fd, t_gnl_save *save, char **line, size_t *size)
{
	char	*line_save;
	size_t	size_save;

	while (read_assign(fd, save->buf, sizeof(save->buf), &save->bytes_read) > 0)
	{
		save->nl_idx = memchridx(save->buf, '\n', (size_t)save->bytes_read);
		if (save->nl_idx != 0)
		{
			line_save = *line;
			size_save = *size;
			*size += save->nl_idx;
			*line = ft_realloc(*line, size_save, *size);
			if (*line == NULL)
			{
				free(line_save);
				return (e_gnl_ret_err);
			}
			ft_memcpy(*line + size_save, save->buf, save->nl_idx);
		}
		if (save->nl_idx != (size_t)save->bytes_read)
			return (e_gnl_ret_found);
	}
	save->nl_idx = 0;
	return (gnl_fd_ret(save, line));
}

static t_gnl_ret	gnl_ret(t_gnl_ret ret, char **line, size_t *size)
{
	char	*line_save;

	line_save = *line;
	*line = malloc(*size + 1);
	if (*line != NULL)
	{
		ft_memcpy(*line, line_save, *size);
		(*line)[*size] = '\0';
	}
	free(line_save);
	if (*line == NULL)
		return (e_gnl_ret_err);
	return (ret);
}

int	get_next_line(int fd, char **line)
{
	static t_gnl_save	saves[GNL_OPEN_MAX];
	size_t				size;
	t_gnl_ret			ret;

	if (fd < 0 || fd >= GNL_OPEN_MAX)
		return (e_gnl_ret_err);
	ret = gnl_save(saves + fd, line, &size);
	if (ret == e_gnl_ret_err)
		return (e_gnl_ret_err);
	if (ret == e_gnl_ret_found)
		return (gnl_ret(e_gnl_ret_found, line, &size));
	ret = gnl_fd(fd, saves + fd, line, &size);
	if (ret == e_gnl_ret_err)
		return (e_gnl_ret_err);
	if (ret == e_gnl_ret_found)
		return (gnl_ret(e_gnl_ret_found, line, &size));
	return (gnl_ret(e_gnl_ret_eof, line, &size));
}
