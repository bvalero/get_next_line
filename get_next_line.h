#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stddef.h>

# include <sys/types.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 4096
# endif
# define GNL_OPEN_MAX 20

typedef enum e_gnl_ret
{
	e_gnl_ret_err = -1,
	e_gnl_ret_eof,
	e_gnl_ret_found
}				t_gnl_ret;

typedef struct s_gnl_save
{
	char	buf[BUFFER_SIZE];
	ssize_t	bytes_read;
	size_t	nl_idx;
}				t_gnl_save;

int		get_next_line(int fd, char **line);

void	*ft_memcpy(void *s, const void *s2, size_t n);
void	*ft_realloc(void *p, size_t size, size_t new_size);
size_t	memchridx(const void *s, int c, size_t n);
ssize_t	read_assign(int fd, void *buf, size_t n, ssize_t *ret);

#endif
