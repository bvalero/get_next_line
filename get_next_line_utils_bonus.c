#include "get_next_line_bonus.h"

#include <stddef.h>
#include <stdlib.h>

#include <unistd.h>

void	*ft_memcpy(void *s, const void *s2, size_t n)
{
	unsigned char		*s3;
	const unsigned char	*s4;
	size_t				cnt;

	s3 = (unsigned char *)s;
	s4 = (const unsigned char *)s2;
	if (s3 == s4)
		return (s);
	cnt = 0;
	while (cnt < n)
	{
		s3[cnt] = s4[cnt];
		++cnt;
	}
	return (s);
}

void	*ft_realloc(void *p, size_t size, size_t new_size)
{
	void	*p2;
	size_t	lesser_size;

	p2 = malloc(new_size);
	if (p2 == NULL)
		return (NULL);
	if (p != NULL)
	{
		if (size <= new_size)
			lesser_size = size;
		else
			lesser_size = new_size;
		ft_memcpy(p2, p, lesser_size);
		free(p);
	}
	return (p2);
}

size_t	memchridx(const void *s, int c, size_t n)
{
	const unsigned char	*s2;
	unsigned char		c2;
	size_t				cnt;

	s2 = (const unsigned char *)s;
	c2 = (unsigned char)c;
	cnt = 0;
	while (cnt < n)
	{
		if (s2[cnt] == c2)
			break ;
		++cnt;
	}
	return (cnt);
}

ssize_t	read_assign(int fd, void *buf, size_t n, ssize_t *ret)
{
	return (*ret = read(fd, buf, n));
}
